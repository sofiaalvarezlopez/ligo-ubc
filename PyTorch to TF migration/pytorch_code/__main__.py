import logging
import os

import numpy as np
import igwn_alert
import torch

from collections import defaultdict

from .commandline import *
from .tree import CNNTree
from .gspynet import GSpyNet2
from .utils import *

CWD = 'src'

if os.path.basename(os.getcwd()) != CWD:
    os.chdir(f'./{CWD}')

# Logging setup
logger = logging.getLogger(__name__)
log_level = getattr(logging, args.log_level)
logger.setLevel(log_level)
ch = logging.StreamHandler()
ch.setLevel(log_level)
fmt = logging.Formatter('%(asctime)s | %(name)s [%(levelname)s] %(message)s')
ch.setFormatter(fmt)
logger.addHandler(ch)
if args.log_file:
    fh = logging.FileHandler(args.log_file)
    fh.setLevel(log_level)
    fh.setFormatter(fmt)
    logger.addHandler(fh)


# GSpyNetTree components
# TODO: Load CNN models
INPUT_SHAPE = (1, 280, 340)
LOWMASS_MODEL = GSpyNet2(input_shape=INPUT_SHAPE, num_classes=5)
LOWMASS_MODEL.load_state_dict(torch.load('./config/lowmass_state_dict.pkl'))
logger.debug('Loaded lowmass classifier.')
HIGHMASS_MODEL = GSpyNet2(input_shape=INPUT_SHAPE, num_classes=5)
# HIGHMASS_MODEL.load_state_dict(torch.load('./config/highmass_state_dict.pkl'))
logger.debug('Loaded highmass classifier.')
VERY_HIGHMASS_MODEL = GSpyNet2(input_shape=INPUT_SHAPE, num_classes=2)
# VERY_HIGHMASS_MODEL.load_state_dict(torch.load('./config/very_highmass_state_dict.pkl'))
logger.debug('Loaded very highmass classifier.')

LO_HI_CLASSIFIERS = CNNTree()
LO_HI_CLASSIFIERS.head.add_child(condition=lambda s: _get_mo(s) < 50, model=LOWMASS_MODEL)
LO_HI_CLASSIFIERS.head.add_child(condition=lambda s: _get_mo(s) >= 50, model=HIGHMASS_MODEL)

VERY_HIGHMASS_CLASSIFIER = CNNTree()
VERY_HIGHMASS_CLASSIFIER.head.add_child(condition=True, model=VERY_HIGHMASS_MODEL)


# Function definitions
def _get_mo(superevent_data):
    '''Get the total mass of a superevent from the data dictionary'''
    return superevent_data['preferred_event_data']['extra_attributes']['CoincInspiral']['mass']


def evaluate_superevent_data(detector, superevent_data):
    '''Run GSpyNetTree on the specified detector for the specified superevent'''
    LO_HI_CLASSIFIERS.set_detector(detector)
    VERY_HIGHMASS_CLASSIFIER.set_detector(detector)
    prediction = LO_HI_CLASSIFIERS.evaluate(superevent_data=superevent_data)
    # Prediction labels are enumerated 0: chirp, 1: low frequency blip
    if _get_mo(superevent_data) >= 50 and np.argmax(prediction) < 2:
        prediction = VERY_HIGHMASS_CLASSIFIER.evaluate(image=LO_HI_CLASSIFIERS._current_image)
    # return prediction
    p_chirp = prediction[0]
    logger.info(detector, prediction, p_chirp)


def evaluate_superevent_gps_time(detector, gps_time, total_mass):
    '''
    Run GSpyNetTree on the specified detector for the specified gps time,
    providing the estimated total mass for the event of interest
    '''
    if gps_time < 0 or total_mass < 0:
        raise ValueError('GPS time and/or total mass cannot be negative')
    superevent_data = defaultdict(dict)
    superevent_data['preferred_event_data']['gpstime'] = gps_time
    superevent_data['preferred_event_data']['extra_attributes']['CoincInspiral']['mass'] = total_mass
    return evaluate_superevent_data(detector, superevent_data)


def process_alert(topic, payload):
    '''
    Handles superevent type igwn-alerts from GraceDB

    Arguments:
    topic -- alert topic (i.e. superevent)
    payload -- data dictionary
    '''
    logger.debug(f'Received alert from {topic}')
    alert_data = payload
    alert_type = alert_data['alert_type']
    if args.alert_types and alert_type not in args.alert_types:
        logger.debug(f'Alert type {alert_type} is not active, skipping')
    else:
        superevent_id = superevent_data["id"]
        superevent_data = alert_data['data']
        logger.debug(
            f'Running GSpyNetTree on alert of type {alert_type} with ID {superevent_id}')
        for detector in ACTIVE_DETECTORS:
            try:
                evaluate_superevent_data(detector, superevent_data)
            except KeyError as err:
                logger.warning(
                    f'Parsing data for {superevent_id} at {detector} encountered {err}')


# Program entrypoint
if __name__ == '__main__':
    logger.debug(f'{args.mode} mode selected.')
    if args.mode == ALERT:
        logger.info('Starting igwn-alert client...')
        logger.debug(f'Authenticating to group {args.group} via {args.auth_server}')
        client = igwn_alert.client(server=args.auth_server, group=args.group)
        logger.debug(
            f'Listening to topics {args.topics} with '
            + (('alert types ' + args.alert_types) if args.alert_types else 'all alert types'))
        client.listen(process_alert, args.topics)
    elif args.mode == MANUAL:
        if not all([a is not None for a in [args.detector, args.gps_time, args.total_mass]]):
            raise ValueError((
                'Insufficient information provided for manual run of GSpyNetTree'
                '- check that you provided a detector, gps time, and total mass estimate.'))
        logger.info((
            f'Running GSpyNetTree on detector {args.detector} at GPS time {args.gps_time}'
            f'for provided total mass estimate of {args.total_mass} M☉'))
        evaluate_superevent_gps_time(args.detector, args.gps_time, args.total_mass)
    elif args.mode == FILE:
        # TODO: figure out file format to load from
        pass
    else:
        raise KeyError(f'Provided unsupported operation mode: {args.mode}')
