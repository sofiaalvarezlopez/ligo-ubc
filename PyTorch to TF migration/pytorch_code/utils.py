import logging
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import skimage

from collections import OrderedDict
from functools import reduce
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import ScalarFormatter
from gwpy.plot import Plot
from gwpy.timeseries import TimeSeries
from gwpy.segments import Segment

logger = logging.getLogger(__name__)

ACTIVE_DETECTORS = ['H1', 'L1', 'V1']
DETECTOR_NAMES = {
    'H1': 'Hanford',
    'L1': 'Livingston',
    'V1': 'Virgo',
    'G1': 'GEO',
    'K1': 'KAGRA'
}

STRAIN_CHANNEL = {
    'H1': 'H1:GDS-CALIB_STRAIN',
    'L1': 'L1:GDS-CALIB_STRAIN',
    'V1': 'V1:Hrec_hoft_16384Hz'
}

EPOCHS = ['O1', 'ER10', 'O2a', 'ER13', 'ER14', 'O3']
TIMES = OrderedDict()
# TIMES[start] = end
TIMES[1126400000] = 1137250000 # O1
TIMES[1161907217] = 1164499217 # ER10
TIMES[1164499217] = 1219276818 # O2a
TIMES[1228838418] = 1229176818 # ER13
TIMES[1235750418] = 1238112018 # ER14
TIMES[1238112018] = 1352505618  # O3
# TODO: Update with ER15 and O4


def fig_to_rgb(fig, xcrop=(66,532), ycrop=(105,671), channels=(0,3)):
    # write plot image to numpy array
    fig.canvas.draw()
    plot_data = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    plot_data = plot_data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    plot_data = plot_data / np.max(plot_data)
    # crop out labels
    if xcrop:
        plot_data = plot_data[xcrop[0]:xcrop[1], :, :]
    if ycrop:
        plot_data = plot_data[:, ycrop[0]:ycrop[1], :]
    if channels:
        plot_data = plot_data[:, :, channels[0]:channels[1]]
    return plot_data


def plot_qtransform(detector_name, start_time, timeseries_path=None,
    plot_normalized_energy_range=[0, 25.5], plot_time_ranges=[0.5, 1, 2, 4],
    qrange=(4, 64), frange=(10, 2048), cmap='viridis', **kwargs):

    # Fetch data for qscans according to requested time ranges
    specsgrams = []
    if timeseries_path:
        ts = TimeSeries.read(timeseries_path)
    else:
        offset = max(plot_time_ranges) + 2
        ts = TimeSeries.get(STRAIN_CHANNEL[detector_name], start_time-offset, start_time+offset)
    for time_window in plot_time_ranges:
        duration_for_plot = time_window/2
        try:
            outseg = Segment(
                start_time - duration_for_plot, start_time + duration_for_plot)
            q_scan = ts.q_transform(
                qrange=qrange,
                frange=frange,
                gps=start_time,
                search=0.5, tres=0.002,
                fres=0.5, outseg=outseg, whiten=True
            )
        except:
            outseg = Segment(
                start_time - 2*duration_for_plot, start_time + 2*duration_for_plot)
            q_scan = ts.q_transform(
                qrange=qrange,
                frange=frange,
                gps=start_time, search=0.5,
                tres=0.002,
                fres=0.5, outseg=outseg, whiten=True
            )
        
        q_scan = q_scan.crop(
            start_time-time_window/2, start_time+time_window/2)
        specsgrams.append(q_scan)

    # Set some plotting params
    myfontsize = 15
    mylabelfontsize = 20
    my_color = 'k'
    
    if detector_name in DETECTOR_NAMES:
        title = DETECTOR_NAMES[detector_name]
    else:
        raise ValueError(
            'You have supplied a detector that is unknown at this time.')

    prev_epoch = None
    this_epoch = None
    next_epoch = None
    for label, (start, end) in zip(EPOCHS, TIMES.items()):
        next_label = label
        if start_time < start:
            break
        elif start_time > end:
            prev_epoch = label
        else:
            this_epoch = label 
            break
    title += '- '
    if this_epoch:
        title += this_epoch
    else:
        if prev_epoch:
            title += f'post {prev_epoch}'
        if prev_epoch and next_epoch:
            title += ' '
        if next_epoch:
            title += f'pre {next_epoch}'

    ind_fig_all = []
    qscans = []

    for i, spec in enumerate(specsgrams):

        ind_fig = spec.plot(figsize=[8, 6], dpi=100)

        ax = ind_fig.gca()
        ax.set_position([0.125, 0.1, 0.775, 0.8])
        
        ## This is where you should change the scaling
        ax.set_yscale('log', base=2) 
        ax.set_xscale('linear')
        ## This is where you should change the scaling
        
        ax.grid(False)

        dur = float(plot_time_ranges[i])

        xticks = np.linspace(spec.xindex.min().value,
                                spec.xindex.max().value, 5)

        xticklabels = []
        for itick in np.linspace(-dur/2, dur/2, 5):
            xticklabels.append(str(itick))

        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)

        ax.set_xlabel('Time (s)', labelpad=0.1, fontsize=mylabelfontsize,
                      color=my_color)
        ax.set_ylabel('Frequency (Hz)', fontsize=mylabelfontsize,
                      color=my_color)
        ax.set_title(title, fontsize=mylabelfontsize, color=my_color)
        ax.title.set_position([.5, 1.05])
        ax.set_ylim(frange)
        ax.yaxis.set_major_formatter(ScalarFormatter())
        ax.ticklabel_format(axis='y', style='plain')

        plt.tick_params(axis='x', which='major', labelsize=myfontsize)
        plt.tick_params(axis='y', which='major', labelsize=12)

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad="3%")

        cbar = ind_fig.colorbar(cax=cax, cmap=cmap,
                                label='Normalized energy',
                                clim=plot_normalized_energy_range)

        cbar.ax.tick_params(labelsize=12)
        cbar.ax.yaxis.label.set_size(myfontsize)

        ind_fig_all.append(ind_fig)
        qscans.append(fig_to_rgb(ind_fig, **kwargs))

    # Create one plot containing all spectograms
    super_fig, axes = plt.subplots(nrows=1, ncols=len(specsgrams),
                                      sharey=True,
                                      subplot_kw={'xscale': 'auto-gps'},
                                      figsize=(27, 6), FigureClass=Plot)
    count = 0

    for iax, spec in zip(axes, specsgrams):
        iax.imshow(spec, cmap=cmap)

        ## This is where you should change the scaling
        iax.set_yscale('log', base=2) 
        iax.set_xscale('linear')
        ## This is where you should change the scaling

        iax.grid(False)

        xticks = np.linspace(spec.xindex.min().value,
                                spec.xindex.max().value, 5)
        dur = float(plot_time_ranges[count])

        xticklabels = []
        for itick in np.linspace(-dur/2, dur/2, 5):
            xticklabels.append(str(itick))

        iax.set_xticks(xticks)
        iax.set_xticklabels(xticklabels)

        iax.set_xlabel('Time (s)', labelpad=0.1, fontsize=mylabelfontsize,
                       color=my_color)
        iax.set_ylim(frange)
        iax.yaxis.set_major_formatter(ScalarFormatter())
        iax.ticklabel_format(axis='y', style='plain')
        iax.colorbar(clim=plot_normalized_energy_range)
        count = count + 1

    super_fig.suptitle(title, fontsize=mylabelfontsize, color=my_color, x=0.51)

    return ind_fig_all, super_fig, qscans


def collate_4_images(images, output_shape=(140,170)):
    if len(images) != 4:
        raise ValueError(f'Expected 4 images; {len(images)} images provided.')

    # Convert to greyscale
    greyscale_images = []
    for image in images:
        if image.shape[-1] != 1:
            image = skimage.color.rgb2gray(image)
            
        image = skimage.transform.resize(image, output_shape, preserve_range=True)
        dim = np.int(reduce(lambda x, y: x * y, image.shape))
        image = np.reshape(image, (dim))
        image = np.array(image, dtype='f')
        greyscale_images.append(image)
    
    # Ensure color channel is first
    reshaped_images = [image.reshape(1, *output_shape) for image in greyscale_images]
    concat_row_axis = -2
    concat_col_axis = -1
    concat_images_set = np.append(reshaped_images[0], reshaped_images[1], axis=concat_row_axis)
    concat_images_set2 = np.append(reshaped_images[2], reshaped_images[3], axis=concat_row_axis)
    
    return np.append(concat_images_set, concat_images_set2, axis=concat_col_axis)
