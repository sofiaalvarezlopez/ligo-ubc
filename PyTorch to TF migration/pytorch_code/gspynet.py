import torch
import torch.nn as nn
import torch.nn.functional as F

class GSpyNet(nn.Module):
    '''Copy of Gravity Spy CNN architecture from Zevin et al (2017)'''
    
    def __init__(self, input_shape, num_classes, dropout_p=0):
        super().__init__()
        channels, width, height = input_shape
        # Filter shapes
        kernel_size1 = 5
        kernel_size2 = 5
        layers = 128
        pool_size = 2
        hidden_nodes = 256
        # Make convolutions
        self.conv1 = nn.Conv2d(
            in_channels=channels, out_channels=layers, kernel_size=kernel_size1)
        self.conv2 = nn.Conv2d(
            in_channels=layers, out_channels=layers, kernel_size=kernel_size2)
        # Make max-pooling
        self.pool = nn.MaxPool2d(kernel_size=pool_size, stride=pool_size)
        # Make dropout layers
        self.dropout = nn.Dropout2d(dropout_p)
        # Make fully connected layers
        adjusted_width = (width - (kernel_size1-1)) // pool_size
        adjusted_width = (adjusted_width - (kernel_size2-1)) // pool_size
        adjusted_height = (height - (kernel_size1-1)) // pool_size
        adjusted_height = (adjusted_height - (kernel_size2-1)) // pool_size
        flat_size = layers * adjusted_width * adjusted_height
        self.fc1 = nn.Linear(in_features=flat_size, out_features=hidden_nodes)
        self.fc2 = nn.Linear(in_features=hidden_nodes, out_features=num_classes)
    

    def forward(self, x):
        # Convolution + Max-pooling
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        # Convolution + Max-pooling + Optional dropout
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = self.dropout(x)
        # Flatten data
        x = torch.flatten(x, 1)
        # Hidden fully connected layer
        x = F.softmax(self.fc1(x))
        # Output layer
        x = self.fc2(x)
        return x

# Class name 
class GSpyNet2(nn.Module):
    '''Copy of Gravity Spy CNN architecture from
    https://github.com/Gravity-Spy/GravitySpy/blob/develop/gravityspy/ml/GS_utils.py#L85'''

    def __init__(self, input_shape, num_classes):
        super().__init__()
        # Make convolutions
        # It looks like the input shape is a tuple/list with three components: # of channels, width, and height of the image.
        channels, width, height = input_shape
        # This is the size of the kernel for the layers.
        kernel_size = 5
        # This is the size of the pool fo the Max Pooling layer
        pool_size = 2
        # These are the sizes of the layers of Gravity Spy.
        layer_sizes = [16, 32, 64, 64]
        # I create a variable to keep track of the channels that enter to each layer and what comes out of it.
        in_channels = channels
        # I create an array to save the convolutional layers
        conv_layers = []
        # I iterate over the layer sizes we previously defined
        for out_channels in layer_sizes:
            # For each of the layer sizes, I add a convolutional layer wtith Relu Activation function.
            # I also create a max pooling layer after each Convolutional Layer
            conv_layers.extend([
                nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size), # Convolutional layer
                nn.ReLU(inplace=True), # Activation function
                nn.MaxPool2d(kernel_size=pool_size, stride=pool_size) # Maxpooling layer
            ])
            in_channels = out_channels # Updating the new input channels for the following layer
        self.features = nn.Sequential(*conv_layers) # Creating a sequential model by 'unwrapping' the convolutional layers
        # Make fully connected layers
        fc_layer_sizes = [256] # Size of the fully connected layer
        adjusted_width = width # Initialize the adjustd width with the width from the input shape
        adjusted_height = height # Initialize the adjusted height with the height from the input shape
        # For each of the sizes I have in the array, I adjust the width and height to calculate the features of the fully connected layer
        for _ in range(len(layer_sizes)): 
            adjusted_width = (adjusted_width - (kernel_size-1)) // pool_size
            adjusted_height = (adjusted_height - (kernel_size-1)) // pool_size
        # We calculate the features for the linear layer
        # Even though this is needed for Pytorch, it is not necessary for Keras.
        in_features = in_channels * adjusted_width * adjusted_height
        # We create an array to store the linear layers
        fc_layers = []
        # We iterate over tbe layer sizes defined (just 1 layer with 256 nodes in this case)
        for hidden_nodes in fc_layer_sizes:
            # We add the linear layer to the net
            fc_layers.extend([
                nn.Linear(in_features=in_features, out_features=hidden_nodes), # Lineal layer with the in and out features
                nn.ReLU(inplace=True) # ReLU function
            ])
            in_features = hidden_nodes # Re-calculation of the input_features according to the number of hidden nodes.
        # Adding the last (output) layer
        fc_layers.append(
            nn.Linear(in_features=fc_layer_sizes[-1], out_features=num_classes)) 
        # We finally save the model into the classifier class attribute.
        self.classifier = nn.Sequential(*fc_layers)
    

    def forward(self, x):
        # Convolution + Max-pooling
        x = self.features(x)
        # Flatten data
        x = torch.flatten(x, 1)
        # Fully connected layers
        x = self.classifier(x)
        return x
