from keras import backend as K
from keras.regularizers import L2
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import MaxPool2D, Conv2D

import torch
import torch.nn as nn
import torch.nn.functional as F

# Definition name 
def GSpyNet2(input_shape, num_classes):
    # Make convolutions
    # This is the size of the kernel for the layers.
    kernel_size = 5
    # This is the size of the pool fo the Max Pooling layer
    pool_size = 2
    # These are the sizes of the layers of Gravity Spy.
    layer_sizes = [16, 32, 64, 64]
    # I create a variable to keep track of the channels that enter to each layer and what comes out of it.
    # I create the sequential model
    classifier = Sequential(name="GSpyNet")
    # I define the regularization parameter
    W_reg = 1e-4
    # I iterate over the layer sizes we previously defined
    for i, filters in enumerate(layer_sizes):
        # For each of the layer sizes, I add a convolutional layer with Relu Activation function.
        if i == 0: # If it is the first layer, I have to tell the input shape to the layer.
            classifier.add(Conv2D(filters=filters, kernel_size=(kernel_size, kernel_size), padding='valid', 
            input_shape=input_shape, activation='relu')) #,kernel_initializer=L2(W_reg)))
            
        else: # If it's not, then I don't specify the input shape.
            classifier.add(Conv2D(filters=filters, kernel_size=(kernel_size, kernel_size), padding='valid', 
            activation='relu')) #, kernel_initializer=L2(W_reg)))
        # I also create a max pooling layer after each Convolutional Layer
        classifier.add(MaxPool2D(pool_size=(pool_size, pool_size), strides=(pool_size, pool_size)))

    # Make fully connected layers
    classifier.add(Flatten()) # We add a flattening layer before the fully connected layer.
    fc_layer_sizes = [256] # Size of the fully connected layer
    units = fc_layer_sizes[0]
    # We iterate over the layer sizes defined (just 1 layer with 256 nodes in this case)
    for hidden_nodes in fc_layer_sizes:
        # We add the linear layer to the net
        classifier.add(Dense(units=units, activation='relu'))#, kernel_regularizer=L2(W_reg)))
            
        units = hidden_nodes # Re-calculation of the input_features according to the number of hidden nodes.
    # Adding the last (output) layer
    classifier.add(Dense(num_classes, activation='softmax'))
    # It may be nice to have the summary of the model
    print(classifier.summary())
    return classifier
    

"""
class GSpyNet(nn.Module):
    '''Copy of Gravity Spy CNN architecture from Zevin et al (2017)'''
    
    def __init__(self, input_shape, num_classes, dropout_p=0):
        super().__init__()
        channels, width, height = input_shape
        # Filter shapes
        kernel_size1 = 5
        kernel_size2 = 5
        layers = 128
        pool_size = 2
        hidden_nodes = 256
        # Make convolutions
        self.conv1 = nn.Conv2d(
            in_channels=channels, out_channels=layers, kernel_size=kernel_size1)
        self.conv2 = nn.Conv2d(
            in_channels=layers, out_channels=layers, kernel_size=kernel_size2)
        # Make max-pooling
        self.pool = nn.MaxPool2d(kernel_size=pool_size, stride=pool_size)
        # Make dropout layers
        self.dropout = nn.Dropout2d(dropout_p)
        # Make fully connected layers
        adjusted_width = (width - (kernel_size1-1)) // pool_size
        adjusted_width = (adjusted_width - (kernel_size2-1)) // pool_size
        adjusted_height = (height - (kernel_size1-1)) // pool_size
        adjusted_height = (adjusted_height - (kernel_size2-1)) // pool_size
        flat_size = layers * adjusted_width * adjusted_height
        self.fc1 = nn.Linear(in_features=flat_size, out_features=hidden_nodes)
        self.fc2 = nn.Linear(in_features=hidden_nodes, out_features=num_classes)
    

    def forward(self, x):
        # Convolution + Max-pooling
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        # Convolution + Max-pooling + Optional dropout
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = self.dropout(x)
        # Flatten data
        x = torch.flatten(x, 1)
        # Hidden fully connected layer
        x = F.softmax(self.fc1(x))
        # Output layer
        x = self.fc2(x)
        return x
"""


