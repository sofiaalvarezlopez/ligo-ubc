import logging
import os
import keras
from keras import backend as K
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
from keras.models import load_model
from tensorflow.keras.optimizers import SGD
import datetime
import numpy as np
import pandas as pd

from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from tqdm.auto import tqdm

#logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)


def dataset_creation(pkl_file, image_shape = [140, 170], data_key=None, label_list=None, order_of_channels="channels_last"):
    """This function creates the dataset in the format expected by the CNN by loading the images from 
    the pickle files."""
    K.set_image_data_format("channels_last")
    # Reading the dataframe stored in the pickle file
    df = pd.read_pickle(pkl_file)
    # Extracting number of rows and columns in the image shape
    img_rows, img_cols = image_shape[0], image_shape[1]
    # TF admits any of the following: 
        # channels_last: (batch_size, imageside1, imageside2, channels)
        # channels_first: (batch_size, channels, imageside1, imageside2)
    # It needs to be done this way because channels_first way doesn't work with CPUs.
    if order_of_channels == "channels_last":
        reshape_order = (-1, img_rows, img_cols, 1)
    else:
        reshape_order = (-1, 1, img_rows, img_cols)
    if data_key is None:
        # All images are reshaped using the reshape_order previously defined.
        # Also, we stack the images.
        image_set1 = np.vstack(df['0.5.png'].values).reshape(reshape_order)
        image_set2 = np.vstack(df['1.0.png'].values).reshape(reshape_order)
        image_set3 = np.vstack(df['2.0.png'].values).reshape(reshape_order)
        image_set4 = np.vstack(df['4.0.png'].values).reshape(reshape_order)

        # Defining the axes to concatenate the datasets. Need to check on this too for TF.
        concat_row_axis = 1 if order_of_channels == "channels_last" else -2  
        concat_col_axis = 2 if order_of_channels == "channels_last" else -1 
            
        # Combining the images into 2 1x2 grids
        concat_images_set = np.append(image_set1, image_set2, axis=concat_row_axis)
        concat_images_set2 = np.append(image_set3, image_set4, axis=concat_row_axis)

        # Concatenating both 1x2 grids into one 2x2 grid
        images = np.append(concat_images_set,concat_images_set2, axis=concat_col_axis)
    # If a data_key is provided, we can directly obtain the images
    else:
        images = np.vstack(df[data_key].values).reshape(reshape_order)
    # Store the tags of the dataframe into a class variable and make the aray 1D.
    tags = np.vstack(df['uniqueID'].values).flatten()
    # Store the labels of the dataframe into a class variable and make the aray 1D.
    str_labels = np.vstack(df['true_label'].values)
    # This is now the label list composed of the true_labels
    # This makes a list of the true_labels having one instance of each in increasing order
    if label_list is None:
        label_list = np.sort(np.unique(str_labels.flatten()))

    # Creating a dictionary with label: glitch key-value pair
    label_num_map = {label: i for i, label in enumerate(label_list)}
    # Converting labels from string to their corresponding numeric representation
    num_labels = np.array([label_num_map[label] for label in str_labels.flatten()])
    # Return a tuple with the images, tags, labels and num_labels
    return images, num_labels, label_list, tags

"""
@params:
save_dir: the directory that contains the model files. 
name: the name of the model.
"""
def get_model_files(save_dir, name):
    """Returns a list of tuples (filename, validation_accuracy)"""
    model_files = []
    for fi in os.listdir(save_dir):
            # Creating a variable with the full path of the model we are currently iterating over.
            full_path = os.path.join(save_dir, fi)
            # Checking if the path corresponds to a file,
            if os.path.isfile(full_path):
                # We check if the file name is of the form {name}_epoch_... and if so, we will append a 3-tuple
                # to the model_files list with an entry of the form (full_path, epoch number, validation accuracy)
                if fi.startswith(f'{name}_epoch_'):
                    val_accuracy = float(full_path.split('_')[-1].split('.')[0])
                    epoch_number = int(full_path.split('-')[0].split('_')[-1])
                    model_files.append((full_path, epoch_number, val_accuracy)) 
    return model_files

"""@params:
model: Model ready to be trained.
images: Images already processed and in the 2x2 required matrices.
label_list: Unique string labels. 
num_labels: Numerical labels corresponding to the str_labels. 
train_size: Percentage of the set that will be used for training.
val_size: Percentage of the set that will be used for validation.
test_size: Percentage of the set that will be used for testing.
epochs: Number of epochs the model will be trained for.
batch: Number of batches that will be used to load the data. 
patience: How many epochs should it wait for before stopping training.
save_improved_epoch: Saves only the epochs in which validation increased, not all of them.
early_stopping: If there should be early stopping in the model training. 
seed: Seed for reproducibility. 
validation_freq: How many training epochs to run before a new validation run is performed.
save_dir: Specifies the directory in which the models should be saved into.
name: Name of the file.
visualize: True if the metrics should be visualized, False if not. 
"""
def train_model(model, images, num_labels, label_list, train_size=0.5, val_size=0.2, test_size=0.3,
                epochs=10, batch=1, patience=10, save_improved_epoch=False, early_stopping=True, seed=27, 
                validation_freq=1, save_dir='models', name='', visualize=True):
    """This function trains the model"""
    # Raises an error if the partition is greater than 1
    # It is important to note that the validation size is taken from the train set.
    if train_size + test_size > 1:
        raise ValueError('Training and test fractions exceeded 1 in total.')
    # Raises an error if there is no test
    if test_size <= 0:
        raise ValueError('Test size must be greater than 0.')
    # Creates a tensor for the neural net to be tested. This creates a categorical array.
    y = keras.utils.np_utils.to_categorical(num_labels, num_classes=len(label_list)) 
    # We can specify the validation size later on training, but we should divide train/test here
    X_train, X_test, Y_train, Y_test = train_test_split(images, y, test_size=test_size, train_size=train_size, random_state=seed, shuffle=True)
    # Compiling the model with an SGD optimizer, a categorical cross-entropy loss function, and with an accuracy metric
    model.compile(loss='categorical_crossentropy', optimizer=SGD(learning_rate=0.001, momentum=0.9), metrics=['accuracy'])
    # Creating an array to store the model files
    # TODO: adapt this to TF.
    model_files = []
    if os.path.exists(save_dir):
        model_files = get_model_files(save_dir, name)
    else:
        os.makedirs(save_dir)
    # We now define the callbacks we will be using:
    # Early Stopping allows the training to be stopped when the monitored metric (validation accuracy, in this case) stops to improve
    # after 10 epochs (in this case).
    early_stopping_callback = EarlyStopping(monitor='val_accuracy', patience=patience, verbose=1, mode='auto', baseline=None)
    # The Model Checkpoint callback saves a model or the weights of a model in a checkpoint file so the model/weights can be loaded
    # later to continue the training from the state saved. I'll save the weights.
    # If all eopchs should be saved, all the models will be stored. 
    # We also create a path to save the model:
    full_checkpoint_path = os.path.join(save_dir, f'{name}_' + 'epoch_{epoch:04d}-val_acc_{val_accuracy:.3f}.hdf5')
    model_checkpoint_callback = ModelCheckpoint(full_checkpoint_path, monitor='val_accuracy', mode='max', verbose=1, 
        save_best_only=save_improved_epoch)
    # Tensorboard enables visualizations of several model metrics. 
    log_dir = os.path.join(save_dir, "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)
    # Creating an array with the callbacks I define
    callbacks = [model_checkpoint_callback, tensorboard_callback]
    # If the user enables early stopping, we also include that callback in the array of callbacks
    if early_stopping: callbacks.append(early_stopping_callback) 
    # Checking that the model files list is not empty to load a model from the ones we already have
    if model_files:
        best_model_path, epoch_number, _ = sorted(model_files, key=lambda acc: acc[-1])[-1]
        print(f'Loading checkpoint for model "{name}" from {best_model_path}')
        logger.info(f'Loading checkpoint for model "{name}" from {best_model_path}')
        best_model = load_model(best_model_path)
        # Initial_epoch = the next to the one in which training was paused. 
        # Apparently the last accuracy is not saved but that should not be a problem while the reloaded accuracy makes sense.
        history = best_model.fit(X_train, Y_train, validation_split=val_size, batch_size=batch, epochs=epochs, 
            validation_freq=validation_freq, verbose=1, initial_epoch=epoch_number, callbacks=callbacks)
    # This happens if we need to train from scratch.
    else:
        print(f'No checkpoints for model "{name}" found, starting training from scratch.')
        logger.info(f'No checkpoints for model "{name}" found, starting training from scratch.')
        # Finally, we fit the model with the X_train and Y_train data and define the validation size
        history = model.fit(X_train, Y_train, validation_split=val_size, batch_size=batch, epochs=epochs, 
            validation_freq=validation_freq, verbose=1, callbacks=callbacks)
        # Saving the history to a file
        #np.save('history.npy',history.history)
    # This is a boolean that displays a plot of training loss and validation accuracy
    if visualize:
        # Creating the plot
        _, ax1 = plt.subplots(1, 1, figsize=(10,6))
        # Plotting the loss of the training set.
        ax1.plot(history.epoch, history.history['loss'], 'k-', label='training loss', alpha=0.3)
        # Making a new curve which shares the x-axis with the previous plot but has its own independent y-axis
        ax2 = ax1.twinx()
        # Plotting the validation accuracy.
        ax2.plot(history.epoch, history.history['val_accuracy'], 'b-', label='validation accuracy')
        # Putting vertical lines in the plot every epoch iteration
        ax1.vlines(history.epoch, *ax1.get_ylim(), colors='k', linestyles='dashed') #, label=f'{save_every_epoch} epoch(s)')
        # Setting the x,y labels of the plots.
        ax1.set_xlabel('Training iteration')
        ax1.set_ylabel('Training loss')
        ax2.set_ylabel('Validation accuracy')
        plt.legend()
        # Saving the plot to the save_dir folder. 
        filename = os.path.join(save_dir, f'{name}_training_plot_' + epoch_number if model_files else '' + '.pdf')
        plt.savefig(filename, bbox_inches='tight')
    # Checking if we should run the model over the test images (in case it isn't empty)
    if test_size > 0:
        # If there is early stopping, then we try to load the model which had the best validation accuracy.
        if early_stopping:
            try:
                model_files = get_model_files(save_dir, name)
                es_model_path = sorted(model_files, key=lambda acc: acc[-1])[-1][0]
                es_loaded_model = load_model(es_model_path)
                score = es_loaded_model.evaluate(X_test, Y_test, verbose=1)
            except:
                print(f'Cannot find best model checkpoint for model {name}.')
                logger.debug(f'Cannot find best model checkpoint for model {name}.')
        # If there was not early stopping, we try to load the last model
        else:
            model_files = get_model_files(save_dir, name)
            latest_model_path = sorted(model_files, key=lambda epoch: epoch[-2])[-1][0]
            latest_model = load_model(latest_model_path)
            score = latest_model.evaluate(X_test, Y_test, verbose=1)
        print('Test accuracy: {0}'.format(score[1]))
        logger.info('Test accuracy: {0}'.format(score[1]))

    return history

def test_model(model, test_images, test_num_labels, name='None', save_dir='data'):
    y_pred = model.predict(test_images)

from gspynet import *
images, num_labels, label_list, tags = dataset_creation('/home/julian.ding/public_html/gspynettree_datasets/low_mass_dataset/low-mass_training_set_combined.pkl')
input_shape = (140*2, 170*2, 1) 
model = GSpyNet2(input_shape, len(label_list)) #GSpyNet2(input_shape, len(dataset.label_map))
train_model(model, images, num_labels, label_list, epochs=1000, save_improved_epoch=True, 
                      train_size=0.9, test_size=0.1,
                      name='gspynet2_lowmass_lowsnr_v3', batch=20, seed=1986)





    
    



    



    






