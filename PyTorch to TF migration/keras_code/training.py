import logging
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset
from tqdm.auto import tqdm

# Log important events
# TODO: Should I consider writing the model compilation message in a log?
logger = logging.getLogger(__name__)

# Key for image-label (later used in dictionary).
IMAGE = 0
LABEL = 1

# Label lists for different classifiers
CHIRP = 'chirp'
BLIP = 'blip'
LF_BLIP = 'low_frequency_blip'
KOI_FISH = 'koi_fish'
TOMTE = 'tomte'
SCRATCHY = 'scratchy'
NO_GLITCH = 'no_glitch'

# Glitches vary depending on source
LOWMASS_LABELS = [CHIRP, LF_BLIP, BLIP, SCRATCHY, NO_GLITCH]
HIGHMASS_LABELS = [CHIRP, LF_BLIP, BLIP, KOI_FISH, TOMTE]
VERY_HIGHMASS_LABELS = [CHIRP, LF_BLIP]

# Info dict keys
EPOCH = 'epoch'
TOTAL_ITER = 'total_iter'
TRAIN_ITERS = 'train_iters'
TRAIN_LOSSES = 'train_losses'
VAL_ITERS = 'val_iters'
VAL_ACCURACY = 'val_accuracy'
EPOCH_ITERS = 'epoch_iters'
STATE_DICT = 'state_dict'
OPTIM_DICT = 'optim_dict'

# Class name
class GSpyDataset(Dataset):
    # Shape of the image 
    image_shape = [140, 170]

    def __init__(self, pkl_path, data_key=None, label_list=None):
        # Reading the dataframe stored in the pickle file 
        df = pd.read_pickle(pkl_path)
        # Storing the image's shape in the image_size variable 
        image_size = self.image_shape
        # Extracting number of rows
        img_rows = image_size[0]
        # Extracting number of columns
        img_cols = image_size[1]
        # Conv2d expects (n_samples, channels, height, width)
        # TODO: Check the expected reshape order for Conv2D in TF. My guess: it should be the same, 
        # but it's better to confirm.
        reshape_order = (-1, 1, img_rows, img_cols)

        # With the current version of the code, this is always executed.
        if data_key is None:
            # All images are reshaped using the reshape_order previously defined.
            # Also, we stack the images.
            image_set1 = np.vstack(df['0.5.png'].values).reshape(reshape_order)
            image_set2 = np.vstack(df['1.0.png'].values).reshape(reshape_order)
            image_set3 = np.vstack(df['2.0.png'].values).reshape(reshape_order)
            image_set4 = np.vstack(df['4.0.png'].values).reshape(reshape_order)

            # Defining the axes to concatenate the datasets. Need to check on this too for TF.
            concat_row_axis = -2
            concat_col_axis = -1
            
            # Combining the images into 2 1x2 grids
            concat_images_set = np.append(image_set1, image_set2, axis=concat_row_axis)
            concat_images_set2 = np.append(image_set3, image_set4, axis=concat_row_axis)
            
            # Concatenating both 1x2 grids into one 2x2 grid
            concat_data = np.append(concat_images_set,concat_images_set2, axis=concat_col_axis)
            # Making the images the class attribute
            self.images = concat_data
        
        # If a data_key is provided, we can directly obtain the images
        else:
            self.images = np.vstack(df[data_key].values).reshape(reshape_order)
        
        # Store the tags of the dataframe into a class variable and make the aray 1D.
        self.tags = np.vstack(df['uniqueID'].values).flatten()
        # Store the labels of the dataframe into a class variable and make the aray 1D.
        str_labels = np.vstack(df['true_label'].values).flatten()

        # This is now the label list composed of the true_labels
        # This makes a list of the true_labels haivng one instance of each (19 or 20 entries for that) in 
        # increasing order
        if label_list is None:
            label_list = np.sort(np.unique(str_labels))
        
        # Making a mapped list of the glitch classes:
        # If we had for example: label_list=['no_glitch', 'tomte', 'koi fish', 'violin_modes', 'scratchy', 'high_mass_chirp', 'low_mass_chirp', 'low_frequency_blip']
        # then:
        # >>> label_num_map
        # {'no_glitch': 0, 'tomte': 1, 'koi fish': 2, 'violin_modes': 3, 'scratchy': 4, 'high_mass_chirp': 5, 'low_mass_chirp': 6, 'low_frequency_blip': 7}
        label_num_map = {label: i for i, label in enumerate(label_list)}

        # This is creating an array of numbers corresponding to the glitches in str_labels
        # We are essentially converting an array of glitch class names which is a string and then converting
        # it to a number so that it can be used successfully in the CNN.
        num_labels = np.array([label_num_map[label] for label in str_labels])
        # We save the numeric labels in a class atribute
        self.labels = num_labels
        # We save the maps of labels (i.e. the numeric label corresponding to each of the types of glitches) in a class attribute.
        # TODO: Ask Julian if it wouldn't be better to use keras.utils.np_utils.to_categorical(Y).
        # However this may cause the data to have a different number assignation.
        self.label_map = label_list
    
    # Returns the amount of qscans (data) in the dataset
    def __len__(self):
        return self.labels.size
    
    # Returns a dictionary with two key-value pairs, one with the image, the other one with its corresponding label.
    def __getitem__(self, i):
        return {IMAGE: self.images[i], LABEL: self.labels[i]}

    # This is a fast way to set items (imagrs and labels) of this class.
    def __setitem__(self, i, value):
        self.images[i] = value[IMAGE]
        self.labels[i] = value[LABEL]

# This is the training pipeline
def train_model(
    # Model and dataset ready to be processed
    model, dataset,
    # Train, validation and test datasets split %
    train_size=0.5, val_size=0.2, test_size=0.3,
    # Number of:
        # epochs and batches
        # Validations after x epochs
    # Each epoch should be saved 
    # Callbacks: there is early stopping
    # Definition of the seed
    # Definition of the directory where the model will be saved
    epochs=10, batch=1, validate_every=5, save_every_epoch=1, early_stopping=True,
    seed=27, device='cuda:0', save_dir='models', name='', visualize=True
):
    # Raises an error if the partition is greater than 1
    if train_size + val_size + test_size > 1:
        raise ValueError('Training, validation, and test fractions exceeded 1 in total.')
  
    # Calculates the number of training, validation and test samples from the dataset according to the previously defined porcentual partitions. 
    num_train = int(train_size * len(dataset))
    num_val = int(val_size * len(dataset))
    num_test = int(test_size * len(dataset))
    # Put remainder into training set
    num_train += len(dataset) - num_train - num_val - num_test
  
    # This is creating the datasets, with the previous number of instances, using a predefined seed.
    # This is equivalent to the sklearn.model_selection.train_test_split() function.
    # TODO: Check if there is an equivalent in TF/Keras. If there's not, should we use sklearn's or can we stick to torch's?
    train, valid, test = torch.utils.data.random_split(
        dataset, [num_train, num_val, num_test], generator=torch.Generator().manual_seed(seed))
    # This is making a dataloader for the training set. This combines a dataset and a sampler
    # and provides an iterable over the given dataset. We can specify how many samples to load
    # per batch and here we specify it to be one. We shuffle the images only for the training set
    train_loader = torch.utils.data.DataLoader(train, batch_size=batch, shuffle=True)
    val_loader = torch.utils.data.DataLoader(valid, batch_size=1)
    test_loader = torch.utils.data.DataLoader(test, batch_size=1)
    # We are going to use the cuda:0 device and this
    model.to(device)
    # The metric to evaluate the CNN is the cross entropy loss 
    criterion = nn.CrossEntropyLoss()
    # We are using Stochastic Gradient Descent (SGD) as the optimizer, with a momentum of 0.9 and a learning rate of 0.001
    optimizer = optim.SGD(
        model.parameters(), lr=0.001, momentum=0.9)
    # Creating an array to store the model files
    model_files = []
    for fi in os.listdir(save_dir):
        # Creating a variable with the full path of the model we are currently iterating over.
        full_path = os.path.join(save_dir, fi)
        # Checking if the path corresponds to a file,
        if os.path.isfile(full_path):
            # We check if the file name is of the form {something}_epoch_... and if so, we will append a 2-tuple
            # to the model_files list with an entry of the form (full_path, epoch #)
            if fi.startswith(f'{name}_epoch_'):
                model_files.append((full_path, int(full_path.split('_')[-1])))
    # Checking that the model files list is not empty
    if model_files:
        # We sort the files by the epochs in ascending order, selecting the last one and then taking the first model of that file.
        latest_model = sorted(model_files, key=lambda a: a[-1])[-1][0]
        # Logging that the model is being loaded.
        logger.info(f'Loading checkpoint for model "{name}" from {latest_model}')
        # Loading the latest model to the cuda:0 device.
        info = torch.load(latest_model, map_location=device)
        # Getting the information of epochs, total iterations, train and validation iterations, train losses, validation accuracy, and epoch iterations.
        # It also loads the statr dict for both the model and the optimizer. 
        epoch = info[EPOCH]
        total_iter = info[TOTAL_ITER]
        train_iters = info[TRAIN_ITERS]
        train_losses = info[TRAIN_LOSSES]
        val_iters = info[VAL_ITERS]
        val_accuracy = info[VAL_ACCURACY]
        epoch_iters = info[EPOCH_ITERS]
        model.load_state_dict(info[STATE_DICT])
        optimizer.load_state_dict(info[OPTIM_DICT])
    else:
        # This happens if the model_files list is empty and training has to be done from scratch.
        # We initialize the number of epochs and total iterations in 0, and create empty lists for all the other variables.
        logger.info(f'No checkpoints for model "{name}" found, starting training from scratch.')
        epoch = 0
        total_iter = 0
        train_iters = []
        train_losses = []
        val_iters = []
        val_accuracy = []
        epoch_iters = []
    # Check whether the training dataloader is not empty (there are training files).
    if len(train_loader) > 0:
        # Will only do this so long as epoch (defined by model) is less than epochs = 10
        # RECALL that epoch is defined as one complete pass of the training dataset through the algorithm.
        # We have defined epochs=10 so that each training dataset goes through the algorithm 10 times
        while epoch < epochs:
            for i, data in tqdm(
                # Creating the necessary parameters for tqdm.
                enumerate(train_loader),
                desc=f'Epoch {epoch+1}/{epochs}',
                total=len(train_loader)
            ):
                # Transfer data to requested device
                inputs = data[IMAGE].to(device)
                labels = data[LABEL].to(device)
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                outputs = model(inputs)
                loss = criterion(outputs, labels)
                # TODO: I'm not exactly sure how to do this with TF. Need to check on this.
                # I think it's just the way PyTorch calculates the gradients.
                loss.backward()
                optimizer.step()
                # keep track of loss
                train_iters.append(total_iter)
                train_losses.append(loss.item())
                total_iter += 1
                # check validation performance
                # We validate every 5 iterations
                if len(val_loader) > 0 and (total_iter+1) % validate_every == 0:
                    # This is function defined later on that provides the number of correctly classified images (from the validation set)
                    # and the total number of images
                    correct, total = predict(model, val_loader, device)
                    # Appeding the amount of iterations
                    val_iters.append(total_iter)
                    # Appending the validation accuracy
                    val_accuracy.append(correct/total)
                    # Trying to obtain the best validation test (We want the best one) and storing it in a file.
                    if len(val_accuracy) == 1 or val_accuracy[-1] > max(val_accuracy[:-1]):
                        filename = os.path.join(save_dir, f'{name}_best_validation')
                        torch.save(model.state_dict(), filename)
            # Add 1 to the epoch
            epoch += 1
            # We save the number of epoch iterations every iteration (save_every_epoch = 1)
            if (epoch % save_every_epoch) == 0:
                epoch_iters.append(total_iter)
            # Structure of the saved file with all of the necessary information about information of the trained model
            if (
                (save_every_epoch and epoch > 0 and (epoch % save_every_epoch) == 0)
                or epoch == epochs
            ):
                filename = os.path.join(save_dir, f'{name}_epoch_{epoch}')
                info = {
                    EPOCH: epoch,
                    TOTAL_ITER: total_iter,
                    TRAIN_ITERS: train_iters,
                    TRAIN_LOSSES: train_losses,
                    VAL_ITERS: val_iters,
                    VAL_ACCURACY: val_accuracy,
                    EPOCH_ITERS: epoch_iters,
                    STATE_DICT: model.state_dict(),
                    OPTIM_DICT: optimizer.state_dict()
                }
                torch.save(info, filename)
            # Doing the prediction for the validation dataset and logging the validation accuracy.
            if len(val_loader) > 0:
                correct, total = predict(model, val_loader, device)
                logger.debug(f'Validation accuracy at the end of epoch {epoch}: {correct/total}')

        # This is just a boolean (set to through or false in the function definition)
        # If we want to visualize the data, the following code will be run
        if visualize:
            # Creating the plot
            fig, ax1 = plt.subplots(1, 1, figsize=(10,6))
            # Plotting the loss of the training set.
            ax1.plot(train_iters, train_losses, 'k-', label='training loss', alpha=0.3)
            # Making a new curve which shares the x-axis with the previous plot but has its own independent y-axis
            ax2 = ax1.twinx()
            # Plotting the validation accuracy.
            ax2.plot(val_iters, val_accuracy, 'b-', label='validation accuracy')
            # Putting vertical lines in the plot every epoch iteration
            ax1.vlines(epoch_iters, *ax1.get_ylim(), colors='k', linestyles='dashed', label=f'{save_every_epoch} epoch(s)')
            # Setting the x,y labels of the plots.
            # TODO: Important metrics to take into account: training loss and validation accuracy.
            ax1.set_xlabel('Training iteration')
            ax1.set_ylabel('Training loss')
            ax2.set_ylabel('Validation accuracy')
            # Saving the plot to the save_dir folder. 
            filename = os.path.join(save_dir, f'{name}_training_plot.pdf')
            plt.savefig(filename, bbox_inches='tight')

    # Checking if we should run the model over the test images (in case it isn't empty)
    if len(test_loader) > 0:
        # If there is early stopping, then we try to load the model which had the best validation accuracy.
        if early_stopping:
            try:
                filename = os.path.join(save_dir, f'{name}_best_validation')
                model.load_state_dict(torch.load(filename, map_location=device))
            except FileNotFoundError:
                logger.debug(f'Cannot find best model state dict for model {name}, continuing with latest.')
        # If there is no early stopping, the last model is used by default. The images are predicted and the metric is printed.
        correct, total = predict(model, test_loader, device)
        logger.info(f'The model "{name}" predicted {correct}/{total} test examples correctly ({round(correct/total, 4)}).')

# Function that makes a prediction
def predict(model, dataloader, device):
    # Need to switch the model off (other layers of the model may be affected by doing this)
    model.eval()
    correct = 0
    total = 0
    # Turns of the gradient computation
    with torch.no_grad():
        # Iterative loop for all the data in the dataloader (test, validation or training dataloader etc)
        for i, data in enumerate(dataloader):
            # Loading the image of the data to inputs (both inputs and labels)
            # TODO: This is actually equivalent to fit in TF.
            inputs = data[IMAGE].to(device)
            labels = data[LABEL].to(device)
            # calculate outputs by running images through the network
            outputs = model(inputs)
            # the class with the highest energy is what we choose as prediction
            _, predicted = torch.max(outputs.data, 1)
            # Calculate the number of correctly labelled items.
            correct += (predicted == labels).sum().item()
            total += labels.size(0)
    # Turn the model back on (so that it can be used as per usual)
    model.train()
    return correct, total


def test_model(model, dataset, device='cuda:0', name=None, save_dir='data'):
    loader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False)
    assert len(loader) == len(dataset)
    prefix = "" if name is None else name + " "

    model.to(device)
    model.eval()
    results = []
    correct = 0
    total = 0
    with torch.no_grad():
        for i, data in tqdm(enumerate(loader), desc=f'{prefix}Test Progress', total=len(loader)):
            image = data[IMAGE].to(device)
            label_i = data[LABEL][0].item()
            true_label = dataset.label_map[label_i]
            output = model(image)[0].cpu().numpy()
            predicted_i = np.argmax(output)
            model_label = dataset.label_map[predicted_i]
            correct += int(predicted_i == label_i)
            total += 1

            results.append([
                dataset.tags[i], image[0].cpu().numpy(),
                label_i, true_label, output, predicted_i, model_label
            ])
    
    logger.info(f'The model "{name}" predicted {correct}/{total} test examples correctly ({round(correct/total, 4)}).')
    columns = [
        'uniqueID', 'image', 'true_label_number', 'true_label',
        'model_output', 'model_label_number', 'model_label'
    ]
    results_df = pd.DataFrame(results, columns=columns)
    outpath = os.path.join(save_dir, f'{prefix}_test_results.pkl')
    logger.info(f'Saving results as {outpath}')
    results_df.to_pickle(outpath)


def confusion_matrix(df, display=False, **kwargs):
    y_pred = df['model_label']
    classes = len(df['model_output'][0])
    y_true = df['true_label']
    if display:
        label_strings = [None for _ in range(classes)]
        for label, string in zip(df['true_label_number'], df['true_label']):
            label_strings[label] = string
        out = ConfusionMatrixDisplay.from_predictions(y_true, y_pred, display_labels=label_strings, **kwargs)
    else:
        out = confusion_matrix(y_true, y_pred, **kwargs)
    return out
