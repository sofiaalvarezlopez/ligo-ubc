import argparse

from .utils import ACTIVE_DETECTORS

print('''   ___ ___           _  _     _  _____            
  / __/ __|_ __ _  _| \| |___| ||_   _| _ ___ ___ 
 | (_ \__ \ '_ \ || | .` / -_)  _|| || '_/ -_) -_)
  \___|___/ .__/\_, |_|\_\___|\__||_||_| \___\___|
          |_|   |__/                              ''') # hehe

ALERT = 'alert'
MANUAL = 'manual'
FILE = 'file'
MODES = [ALERT, MANUAL, FILE]

# Commandline argument definitions
parser = argparse.ArgumentParser(
    description='GSpyNetTree: Gravity Spy Convolutional Neural Network Decision Tree',
    epilog='Classifier for glitch-chirp discrimination based on strain data qscans.'
)
parser.add_argument(
    '--log-level', dest='log_level', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'], default='DEBUG',
    help='set log level')
parser.add_argument(
    '--log-file', dest='log_file', default=None, help='write logs to file')

parser.add_argument(
    '--mode', dest='mode', choices=MODES, default=ALERT,
    help=(
        'specify operation mode - '
        f'{ALERT}: listen for real-time GraceDB alerts; '
        f'{MANUAL}: supply an event (detector, gps time, total mass) manually through commandline; '
        f'{FILE}: read GraceDB superevent data from local file'))
parser.add_argument(
    '--auth-server', dest='auth_server', default='kafka://kafka.scimma.org/',
    help=f'[{ALERT}] specify alternate auth server URL (default: kafka.scimma.org)')
parser.add_argument(
    '--group', dest='group', choices=['gracedb', 'gracedb-playground', 'gracedb-test'],
    default='gracedb-playground', help=f'[{ALERT}] specify igwn-alert client group to listen to')
parser.add_argument(
    '--topics', dest='topics', nargs='+', default=['superevent', 'mdc_superevent', 'test_superevent'],
    help=f'[{ALERT}] specify igwn-alert client topics to subscribe to (make sure you have the right permissions set up!)')
parser.add_argument(
    '--alert-types', dest='alert_types', nargs='+', default=[],
    help=f'[{ALERT}] specify GraceDB alert types to run on (default: all)')
parser.add_argument(
    '--detector', dest='detector', choices=ACTIVE_DETECTORS, default=None,
    help=f'[{MANUAL}] specify target detector for event of interest')
parser.add_argument(
    '--gps-time', dest='gps_time', type=float, default=None,
    help=f'[{MANUAL}] specify GPS time for event of interest')
parser.add_argument(
    '--total-mass', dest='total_mass', type=float, default=None,
    help=f'[{MANUAL}] specify CBC total mass for event of interest')

args = parser.parse_args()
